import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const CategoryGridTile = (props) => {
  return (
    <TouchableOpacity style={styles.griditem} onPress={props.onSelect}>
      <View style={{...styles.screen, ...{backgroundColor: props.color}}}>
        <Text style={styles.title} numberOfLines={2}>
          {props.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default CategoryGridTile;
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    borderRadius: 10,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    elevation: 3,
    padding: 10,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  title: {
    fontSize: 22,
  },
  griditem: {
    flex: 1,
    margin: 15,
    height: 150,
  },
});
