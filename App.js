/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import MealsNavigator from './routes/MealsNavigator';
import mealsReducer from './store/reducers/meals';

const rootReducer = combineReducers({
  meals: mealsReducer,
});
const store = createStore(rootReducer);

function App() {
  return (
    <Provider store={store}>
      <MealsNavigator />
    </Provider>
  );
}

export default App;
