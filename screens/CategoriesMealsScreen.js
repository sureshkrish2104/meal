import React from 'react';
import {View, Text, StyleSheet, Button, Platform, FlatList} from 'react-native';
import {useSelector} from 'react-redux';
import Colors from '../constants/Colors';
import {CATEGORIES, MEALS} from '../data/dummy-data';
import MealList from '../components/MealList';

const CategoriesMealsScreen = (props) => {
  const item = props.navigation.getParam('id');
  const availableMeals = useSelector((state) => state.meals.filteredMeals);
  console.warn('available', availableMeals);
  const displayedMeals = availableMeals.filter(
    (meal) => meal.categoryIds.indexOf(item) >= 0,
  );
  if (displayedMeals.length === 0) {
    return (
      <View style={styles.screen}>
        <Text>No meals found,maybe please check filters?</Text>
      </View>
    );
  }

  return <MealList listdata={displayedMeals} navigation={props.navigation} />;
};
CategoriesMealsScreen.navigationOptions = (navigationData) => {
  const item = navigationData.navigation.getParam('id');
  return {
    headerTitle: item.title,
  };
};
export default CategoriesMealsScreen;
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
