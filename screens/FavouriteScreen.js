import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';
import MealList from '../components/MealList';
import {MEALS} from '../data/dummy-data';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton';

const FavouriteScreen = (props) => {
  const favmeals = useSelector((state) => state.meals.favoriteMeals);
  if (favmeals.length === 0 || !favmeals) {
    return (
      <View style={styles.content}>
        <Text>No favourite meals found.Start adding some</Text>
      </View>
    );
  }
  return <MealList listdata={favmeals} navigation={props.navigation} />;
};
FavouriteScreen.navigationOptions = (navData) => {
  return {
    headerTitle: 'Your Favourites',
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
  };
};
export default FavouriteScreen;
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
