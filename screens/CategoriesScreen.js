import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import {CATEGORIES} from '../data/dummy-data';
import HeaderButton from '../components/HeaderButton';

import Colors from '../constants/Colors';
import CategoryGridTile from '../components/CategoryGridTile';

const CategoriesScreen = (props) => {
  const renderGriditem = (itemdata) => {
    return (
      <CategoryGridTile
        title={itemdata.item.title}
        color={itemdata.item.color}
        onSelect={() => {
          props.navigation.navigate('CategoryMeals', {
            id: itemdata.item.id,
          });
        }}
      />
    );
  };
  return (
    <FlatList
      keyExtractor={(item, index) => item.id}
      data={CATEGORIES}
      renderItem={renderGriditem}
      numColumns={2}
    />
  );
};
CategoriesScreen.navigationOptions = (navData) => {
  return {
    headerTitle: 'Meal Categories',
    headerStyle: {
      backgroundColor: Platform.OS === 'android' ? Colors.primaryColor : '',
    },
    headerTintColor: 'white',
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
  };
};
export default CategoriesScreen;
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  griditem: {
    flex: 1,
    margin: 15,
    height: 150,
  },
});
